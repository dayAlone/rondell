(function() {
  var calcLayout, checkCollection, checkKnight, checkScroll, checkSliders, checkTable, delay, detailClose, dropdownChange, end, fromPx, mapStyle, openGallery;

  delay = function(ms, func) {
    return setTimeout(func, ms);
  };

  end = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd';

  mapStyle = [
    {
      stylers: [
        {
          visibility: "on"
        }, {
          saturation: -100
        }, {
          lightness: 30
        }
      ]
    }, {
      featureType: "administrative.country",
      elementType: "labels",
      stylers: [
        {
          weight: .1
        }, {
          visibility: "off"
        }, {
          color: "#ffffff"
        }
      ]
    }, {
      featureType: "administrative",
      elementType: "geometry",
      stylers: [
        {
          visibility: "on"
        }, {
          weight: .4
        }, {
          color: "#646464"
        }
      ]
    }, {
      featureType: "poi.school",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }, {
      featureType: "road.highway",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff"
        }, {
          visibility: "simplified"
        }
      ]
    }, {
      featureType: "road.highway",
      elementType: "labels.text",
      stylers: [
        {
          weight: .1
        }, {
          color: "#ffffff"
        }, {
          visibility: "on"
        }
      ]
    }, {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff"
        }, {
          visibility: "simplified"
        }
      ]
    }, {
      featureType: "road.arterial",
      elementType: "labels",
      stylers: [
        {
          weight: .1
        }, {
          color: "#000"
        }, {
          visibility: "on"
        }
      ]
    }, {
      featureType: "road.local",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff"
        }
      ]
    }, {
      featureType: "road",
      elementType: "labels",
      stylers: [
        {
          color: "#757575"
        }
      ]
    }, {
      featureType: "transit.station",
      elementType: "labels.icon",
      stylers: [
        {
          hue: "#8800ff"
        }, {
          visibility: "on"
        }, {
          saturation: 5
        }
      ]
    }, {
      featureType: "road.highway",
      elementType: "labels.icon",
      stylers: [
        {
          weight: .1
        }, {
          saturation: 11
        }, {
          lightness: 50
        }, {
          visibility: "off"
        }
      ]
    }, {
      featureType: "administrative.locality",
      elementType: "labels.text",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }, {
      featureType: "transit.station",
      elementType: "labels.text",
      stylers: [
        {
          visibility: "on"
        }, {
          weight: .1
        }, {
          color: "#323232"
        }
      ]
    }, {
      featureType: "transit.station.bus",
      elementType: "labels.icon",
      stylers: [
        {
          gamma: .72
        }, {
          weight: .1
        }, {
          saturation: 77
        }, {
          lightness: 1
        }, {
          hue: "#0099ff"
        }
      ]
    }, {
      featureType: "transit.station",
      elementType: "labels.text",
      stylers: [
        {
          visibility: "on"
        }, {
          weight: .1
        }, {
          color: "#3c3c3c"
        }
      ]
    }, {
      elementType: "labels.text.stroke",
      stylers: [
        {
          visibility: "on"
        }, {
          weight: .1
        }, {
          color: "#464646"
        }
      ]
    }, {
      featureType: "administrative.land_parcel",
      elementType: "labels.text",
      stylers: [
        {
          visibility: "on"
        }, {
          color: "#3c3c3c"
        }
      ]
    }, {
      featureType: "water",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }, {
      featureType: "water",
      elementType: "geometry.fill",
      stylers: [
        {
          visibility: "on"
        }, {
          color: "#eeeeee"
        }
      ]
    }, {
      featureType: "road",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }, {
      featureType: "administrative.country",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }
  ];

  fromPx = function(val) {
    return parseInt(val.replace('px', ''));
  };

  checkKnight = function() {
    var $elem, $img, $text, $title, top;
    if ($('.history').length > 0) {
      $elem = $('.history').byMod('knight');
      $img = $elem.elem('image');
      $text = $elem.elem('description');
      $title = $('.text h3');
      top = $img.height() - $text.height() - fromPx($elem.css('paddingBottom'));
      $img.css({
        marginTop: "-" + (top - 2) + "px",
        marginBottom: "-" + ($elem.css('paddingBottom'))
      });
      if ($(window).width() > 340 && $(window).width() < 768) {
        return $title.css({
          maxWidth: $elem.width() - $img.width() / 2
        });
      } else if ($(window).width() >= 768) {
        return $title.css({
          maxWidth: $elem.width() - $img.width() + 30
        });
      }
    }
  };

  checkScroll = function() {
    return $('.scroll').each(function(key, el) {
      $(el).mod('ready', $(el).find('.scroll__wrap').outerHeight() > $(el).outerHeight());
      if ($(el).find('.scroll__content').length > 0) {
        el = $(el).find('.scroll__content');
      }
      if ($(window).width() >= 768 && !$.browser.mobile) {
        if ($(el).hasClass('ps-container')) {
          $(el).perfectScrollbar('update');
        }
        if ($(el).find('.scroll__wrap').outerHeight() > $(el).outerHeight()) {
          if (!$(el).hasClass('ps-container')) {
            return $(el).perfectScrollbar({
              suppressScrollX: true,
              includePadding: true
            });
          }
        }
      }
    });
  };

  checkSliders = function() {
    return $('.slider').byMod('relative').each(function(key, el) {
      var heights;
      heights = [];
      $(el).removeAttr('style');
      $(el).parents('.content__slider').removeAttr('style');
      return delay(300, function() {
        var max;
        $(el).elem('item').each(function(key, item) {
          var height;
          if ($.browser.mobile) {
            height = $(item).find('.slider__content').outerHeight() + $(item).find('.slider__image').outerHeight();
          } else {
            height = Math.max($(item).outerHeight(), $(item).find('.slider__content').outerHeight());
          }
          if (height > $(el).height()) {
            return heights.push(height);
          }
        });
        if (heights.length > 0) {
          max = Math.max.apply(null, heights);
          $(el).css({
            minHeight: max
          });
          return $(el).parents('.content__slider').css({
            minHeight: max
          });
        }
      });
    });
  };

  checkCollection = function(el) {
    if ($('.collection').hasMod('open')) {
      if (!$.browser.mobile) {
        $('.collection').elem('image').height($('.collection').elem('content').height());
      }
      $('.collection__descriptions').css({
        minHeight: $('.collection__description--full').height()
      });
      return $('.collection').elem('trigger').text('Свернуть');
    } else {
      $('.collection').elem('trigger').text('Подробнее');
      return $('.collection__descriptions').css({
        minHeight: 0
      });
    }
  };

  checkTable = function(el) {
    var fullHeight, smallHeight;
    smallHeight = 0;
    fullHeight = 0;
    $('.product').elem('table').find('.product__row').each(function(key, el) {
      if (key < 5) {
        smallHeight += $(el).outerHeight() + 1;
      }
      return fullHeight += $(el).outerHeight() + 1;
    });
    if ($('.product').hasMod('open')) {
      return $('.product').elem('table').css({
        maxHeight: fullHeight
      });
    } else {
      return $('.product').elem('table').css({
        maxHeight: smallHeight
      });
    }
  };

  calcLayout = function() {
    checkKnight();
    checkScroll();
    checkSliders();
    checkCollection();
    return checkTable();
  };

  detailClose = function() {
    $('.page').mod('detail', false);
    $(window).scrollTop($('.page').elem('content').data('scroll'));
    $('.page').elem('content').removeAttr('style');
    $('.detail-list').mod('active', false);
    return $('.nav-trigger').mod('open', false);
  };

  dropdownChange = function(block, text) {
    var select, selected, trigger, value;
    trigger = block.elem('trigger').find('span');
    select = block.elem('select');
    if (select) {
      selected = select.find("option[data-value='" + text + "']");
      select.find("option:selected").removeAttr('selected');
      select[0].selectedIndex = selected.index();
      selected.attr('selected', 'selected');
    }
    trigger.text(text);
    block.addClass('dropdown--inactive');
    delay(2000, function() {
      return block.removeClass('dropdown--inactive');
    });
    if (block.attr('onChange') && block.attr('onChange').length > 0) {
      value = selected.attr('value');
      return eval((block.attr('onChange')) + "(text, value)");
    }
  };

  openGallery = function(items, index) {
    var elem, gallery, options;
    elem = $('.pswp')[0];
    options = {
      history: false,
      focus: false,
      shareEl: false
    };
    if (index > 0) {
      options.index = index;
    }
    gallery = new PhotoSwipe(elem, PhotoSwipeUI_Default, items, options);
    return gallery.init();
  };

  $(document).ready(function() {
    var checkBigImage;
    if ($.browser.mobile) {
      $('html').addClass('mobile');
    }
    $('html').addClass($.browser.name + $.browser.versionNumber);
    $('html').addClass($.browser.platform);
    if ($.browser["windows phone"]) {
      $('html').addClass('win');
    }
    $.BEM = new $.BEM.constructor({
      namePattern: '[a-zA-Z0-9-]+',
      elemPrefix: '__',
      modPrefix: '--',
      modDlmtr: '--'
    });
    $(window).on('resize', _.throttle(calcLayout, 300));
    delay(300, calcLayout);
    delay(500, function() {
      return $('.slider').slick({
        arrows: false,
        autoplay: true,
        speed: 500,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        dots: true,
        fade: true,
        dotsClass: 'slider__dots',
        loop: true
      });
    });
    if ($(window).width() >= 768) {
      $('.products--slider').slick({
        speed: 500,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        loop: true,
        nextArrow: '<div class="slick-next"></div>',
        prevArrow: '<div class="slick-prev"></div>',
        responsive: [
          {
            breakpoint: 2800,
            settings: {
              slidesToShow: 4
            }
          }, {
            breakpoint: 1280,
            settings: {
              slidesToShow: 3
            }
          }, {
            breakpoint: 960,
            settings: {
              slidesToShow: 2
            }
          }
        ]
      });
    }
    $('.dropdown').elem('trigger').on('click', function(e) {
      return e.preventDefault();
    });
    $('.dropdown').elem('trigger').on('mouseenter', function(e) {
      return $(this).block().mod('inactive', false);
    });
    $('.dropdown').elem('item').on('click', function(e) {
      dropdownChange($(this).block(), $(this).text());
      return e.preventDefault();
    });
    $('.dropdown').elem('select').on('change', function(e) {
      dropdownChange($(this).block(), $(this).find('option:selected').data('value'));
      return e.preventDefault();
    });
    $('.toolbar__search, .sidebar__search').on('click', function(e) {
      $('.page').mod('search', true);
      $('.search').mod('active', true);
      $('.search input').focus();
      return e.preventDefault();
    });
    $('.search__fade, .search__close').on('click', function(e) {
      $('.page').mod('search', false);
      return $('.search').mod('active', false);
    });
    $('.nav-trigger').on('click', function(e) {
      if (!$('.page').hasMod('detail')) {
        $('.page').mod('nav', !$('.page').hasMod('nav'));
        $('.nav').mod('active', !$('.nav').hasMod('active'));
        $(this).mod('open', !$(this).mod('open'));
      } else {
        detailClose();
      }
      return e.preventDefault();
    });
    $('.nav__trigger').on('click', function(e) {
      var el;
      el = $(this).parents('.nav__section');
      el.mod('open', !el.hasMod('open'));
      return e.preventDefault();
    });
    $('.guarantee-trigger').on('click', function(e) {
      $(this).addClass('hidden');
      $('.guarantee-content').removeClass('hidden');
      return e.preventDefault();
    });
    if (window.location.hash === '#unguarantee') {
      $('.guarantee-trigger').addClass('hidden');
      $('.guarantee-content').removeClass('hidden');
    }
    $('.detail-list').elem('close').on('click', function(e) {
      detailClose();
      return e.preventDefault();
    });
    $('[data-detail]').on('click', function(e) {
      $('.page').elem('content').data('scroll', $(window).scrollTop()).css({
        top: -$(window).scrollTop() + $('.page').elem('content').offset().top
      });
      $('.page').elem('content').scrollTop(0);
      $('.page').mod('detail', true);
      $('.detail-list').mod('active', true);
      $('.nav-trigger').mod('open', true);
      return e.preventDefault();
    });
    $('.collection').elem('trigger').on('click', function(e) {
      $(this).block().mod('open', !$(this).block().hasMod('open'));
      checkCollection();
      return e.preventDefault();
    });
    $('[data-filter-trigger]').on('click', function(e) {
      $(this).parents('.filter').addClass('filter--opened');
      return e.preventDefault();
    });
    $('[data-table]').on('click', function(e) {
      $('.product').mod('open', !$('.product').hasMod('open'));
      checkTable();
      if ($('.product').hasMod('open')) {
        $(this).find('span').text('Скрыть');
      } else {
        $(this).find('span').text('Все характеристики');
      }
      return e.preventDefault();
    });
    checkBigImage = function() {
      var id;
      id = $('.slick-slide.current').find('.product__thumbnail').data('id');
      $(".product__big").removeClass('product__big--active');
      return $(".product__big[data-id='" + id + "']").addClass('product__big--active');
    };
    $('.product__thumbnail').on('click', function(e) {
      $('.product__thumbnails .current').removeClass('current');
      $(this).parent().addClass('current');
      checkBigImage();
      return e.preventDefault();
    });
    $('.product__thumbnails').on('init', (function(e, slick) {
      $('.product__thumbnails .slick-slide.slick-current').addClass('current');
      return $('.product__thumbnails .slick-arrow').off('click').on('click', (function(e) {
        var current, currentIndex, first, last, next, showFirst, showLast, shown;
        current = $('.product__thumbnails .slick-slide.current').data('slick-index');
        shown = $('.product__thumbnails .slick-slide.slick-active').length;
        showFirst = $('.product__thumbnails .slick-slide.slick-active:first').data('slick-index');
        showLast = $('.product__thumbnails .slick-slide.slick-active:last').data('slick-index');
        first = $('.product__thumbnails .slick-slide:first').data('slick-index');
        last = $('.product__thumbnails .slick-slide:last').data('slick-index');
        if ($(this).hasClass('slick-next')) {
          next = current + 1;
          if (next > last) {
            next = first;
          }
          if (current === showLast) {
            $('.product__thumbnails').slick('slickGoTo', next);
            $('.product__thumbnails').one('afterChange', (function() {
              $('.product__thumbnails .slick-slide').removeClass('current');
              $('.product__thumbnails .slick-slide.slick-current').addClass('current');
              return checkBigImage();
            }));
          } else {
            $('.product__thumbnails .slick-slide').removeClass('current');
            $('.product__thumbnails .slick-slide[data-slick-index="' + next + '"]').addClass('current');
          }
        } else if ($(this).hasClass('slick-prev')) {
          if (next < first) {
            next = last;
          }
          if (current === showFirst) {
            currentIndex = $('.product__thumbnails .slick-slide[data-slick-index="' + current + '"]').index();
            next = currentIndex - shown;
            if (next < 1) {
              next = last - shown - 1;
            } else {
              next = $('.product__thumbnails .slick-slide:nth-child(' + next + ')').data('slick-index');
            }
            $('.product__thumbnails').one('afterChange', (function() {
              $('.product__thumbnails .slick-slide').removeClass('current');
              $('.product__thumbnails .slick-slide.slick-active:last').addClass('current');
              return checkBigImage();
            }));
            $('.product__thumbnails').slick('slickGoTo', next);
          } else {
            next = current - 1;
            $('.product__thumbnails .slick-slide').removeClass('current');
            $('.product__thumbnails .slick-slide[data-slick-index="' + next + '"]').addClass('current');
          }
        }
        checkBigImage();
        return e.preventDefault();
      }));
    }));
    delay(500, function() {
      return $('.product__thumbnails').slick({
        speed: 500,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        loop: true,
        nextArrow: '<div class="slick-next"></div>',
        prevArrow: '<div class="slick-prev"></div>',
        responsive: [
          {
            breakpoint: 2800,
            settings: {
              slidesToShow: 4
            }
          }, {
            breakpoint: 1280,
            settings: {
              slidesToShow: 3
            }
          }, {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });
    });
    $('.product').elem('zoom').on('click', function(e) {
      var items;
      items = $('.product__images').data('images');
      openGallery(items, $('.product__big--active').data('id') - 1);
      return e.preventDefault();
    });
    $('.product__big').each(function(key, el) {
      return $(el).zoom({
        url: $(el).data('big'),
        magnify: 2,
        target: $('.product__hover')[0]
      });
    });
    $('.artnumbers').elem('photo').on('click', function(e) {
      openGallery($(this).data('images'));
      return e.preventDefault();
    });
    $('.cities__link').on('click', function(e) {
      var text, value;
      if (window.geocoder && window.map) {
        $('#cities').modal('hide');
        text = $(this).text();
        value = $(this).data('id');
        eval(($('.map').attr('onCityChange')) + "(text, value)");
        window.geocoder.geocode({
          'address': text
        }, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results) {
              $('.city-select__name').text(text);
              window.map.setCenter(results[0].geometry.location);
              return window.map.setZoom(10);
            }
          }
        });
      }
      return e.preventDefault();
    });
    if ($('.map').length > 0) {
      $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyBoQpohaOwuvBpUTLSbU299yS6gjMdG3ys', function() {
        window.geocoder = new google.maps.Geocoder();
        return $('.map').each(function(key, el) {
          var center, clusterStyle, geocoder, infoBubble, items, map, marker, markerCluster, markers, tmp, value;
          center = $(el).data('center').split(', ');
          map = new google.maps.Map($(el)[0], {
            zoom: $(el).data('zoom') || 10,
            styles: mapStyle,
            center: {
              lat: parseFloat(center[0]),
              lng: parseFloat(center[1])
            }
          });
          window.map = map;
          tmp = $(el).data('items');
          items = [];
          for (key in tmp) {
            value = tmp[key];
            items.push(value);
          }
          if (items.length > 0) {
            geocoder = new google.maps.Geocoder();
            clusterStyle = [
              {
                height: 60,
                width: 60,
                anchor: [30, 0]
              }
            ];
            markers = [];
            infoBubble = new InfoBubble({
              map: map,
              shadowStyle: 0,
              padding: 0,
              arrowSize: 0,
              disableAutoPan: true,
              hideCloseButton: true,
              arrowStyle: 0,
              backgroundColor: '#515255',
              borderColor: '#515255',
              borderRadius: 0,
              color: '#fff',
              padding: 10,
              minWidth: 200,
              minHeight: 100
            });
            $.each(items, function(k, i) {
              var marker;
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(i.coords[0], i.coords[1]),
                icon: {
                  url: "/layout/images/pin-small.png",
                  size: new google.maps.Size(58, 80),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(14, 40),
                  scaledSize: new google.maps.Size(29, 40)
                },
                animation: google.maps.Animation.DROP
              });
              marker.addListener('click', function() {
                infoBubble.setContent("<div class='map__bubble'><strong>" + i.name + "</strong><br/>Телефон: " + i.phone + "<br/>Адрес: " + i.address + "</div>");
                infoBubble.open(map, marker);
                return delay(30, function() {
                  var parent;
                  parent = $('.map__bubble').parents("[class*='ibani']");
                  if (!parent.hasClass('map__popup')) {
                    parent.addClass('map__popup');
                  }
                  return infoBubble.setMinHeight($('.map__bubble').outerHeight() + 25);
                });
              });
              return markers.push(marker);
            });
            return markerCluster = new MarkerClusterer(map, markers, {
              cssClass: 'map__cluster',
              styles: clusterStyle,
              gridSize: 60,
              maxZoom: 13
            });
          } else {
            return marker = new google.maps.Marker({
              position: new google.maps.LatLng(parseFloat(center[0]), parseFloat(center[1])),
              map: map,
              icon: {
                url: "/layout/images/pin.png",
                size: new google.maps.Size(90, 124),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(23, 62),
                scaledSize: new google.maps.Size(45, 62)
              },
              animation: google.maps.Animation.DROP
            });
          }
        });
      });
    }
    $('.form__file a').click(function(e) {
      $(this).parent().find('input[type=file]').trigger('click');
      return e.preventDefault();
    });
    $('input[type=file]').on('change', function() {
      return $(this).parent().find('.form__label').text($(this).val().replace(/.+[\\\/]/, ""));
    });
    $('.modal').on('shown.bs.modal', function() {
      return checkScroll();
    });
    return $('.scroll, .scroll__content').on('scroll', _.throttle((function() {
      var el, parent;
      el = $(this).find('.scroll__wrap');
      parent = $(this);
      if ($(this).parents('.scroll').length > 0) {
        parent = $(this).parents('.scroll');
      }
      parent.toggleClass('scroll--start', $(this).scrollTop() > 0);
      return parent.toggleClass('scroll--end', el.outerHeight() <= $(this).scrollTop() + $(this).outerHeight());
    }), 100));
  });

}).call(this);
